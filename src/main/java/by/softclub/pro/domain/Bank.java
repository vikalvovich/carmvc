package by.softclub.pro.domain;

import lombok.Data;

@Data
public class Bank {
    private String name;
}

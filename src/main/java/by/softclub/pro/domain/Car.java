package by.softclub.pro.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
//@AllArgsConstructor
//@NoArgsConstructor
public class Car {
    private int id;
    private String model;
    private int price;
    private String image;

    public String getImage() {
        return model.toLowerCase() + ".jpg";
    }

}

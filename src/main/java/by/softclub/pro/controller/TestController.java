package by.softclub.pro.controller;

import by.softclub.pro.domain.Bank;
import by.softclub.pro.domain.Car;
import by.softclub.pro.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    private TestService testService;

    @GetMapping(value = "/test")
    public Car getCar() {
        return Car.builder()
                .id(12)
                .model("Chevrolet")
                .price(80)
                .build();
    }

    @GetMapping(value = "/bank")
    public Bank getBank() {
        Bank bank = new Bank();
        bank.setName(testService.getInfo());
        return bank;
    }
}

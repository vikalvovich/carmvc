package by.softclub.pro.controller;

import by.softclub.pro.domain.Car;
import by.softclub.pro.service.CarService;
import by.softclub.pro.service.HistoryService;
import by.softclub.pro.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class StorageController {

    @Autowired
    private CarService carService;

    @Autowired
    private StorageService storageService;

    @Autowired
    private HistoryService historyService;

    @GetMapping("/storage/list")
    public String storageList(Model model) {
        model.addAttribute("cars", storageService.getList());
        return "storage";
    }

    @PostMapping("/storage/buy")
    public String buy(@RequestParam("car") int id, Model model) {
        Car car = storageService.getById(id);
        storageService.deleteById(id);
        historyService.add(car);
        model.addAttribute("cars", storageService.getList());
        return "storage";
    }

    @PostMapping("/storage/delete")
    public String delete(@RequestParam("car") int id, Model model) {
        Car car = storageService.getById(id);
        storageService.deleteById(id);
        carService.add(car);
        model.addAttribute("cars", storageService.getList());
        return "storage";
    }
}

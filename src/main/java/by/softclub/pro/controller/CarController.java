package by.softclub.pro.controller;

import by.softclub.pro.domain.Car;
import by.softclub.pro.service.CarService;
import by.softclub.pro.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CarController {

    @Autowired
    private CarService carService;

    @Autowired
    private StorageService storageService;

    @GetMapping("/car/list")
    public String carList(Model model) {
        model.addAttribute("cars", carService.getList());
        return "car";
    }

    @PostMapping("/car")
    public String addCarInStorage(@RequestParam("car") int id, Model model) {
        Car car = carService.getById(id);
        carService.deleteById(id);
        storageService.add(car);
        model.addAttribute("cars", carService.getList());
        return "car";
    }
}

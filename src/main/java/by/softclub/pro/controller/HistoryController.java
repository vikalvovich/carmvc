package by.softclub.pro.controller;

import by.softclub.pro.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HistoryController {

    @Autowired
    private HistoryService historyService;

    @GetMapping("/history/list")
    public String storageList(Model model) {
        model.addAttribute("cars", historyService.getList());
        return "history";
    }

}

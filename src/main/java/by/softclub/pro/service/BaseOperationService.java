package by.softclub.pro.service;

import by.softclub.pro.domain.Car;

import java.util.List;

public interface BaseOperationService<T> {
    void add(T object);
    T getById(int id);
    void deleteById(int id);
    List<Car> getList();
}

package by.softclub.pro.service;

import by.softclub.pro.domain.Car;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class CarServiceImpl implements CarService {

    private List<Car> cars = new ArrayList<>();

    @PostConstruct
    public void init() {
        var random = new Random();
        cars.add(
                Car.builder()
                        .id(1)
                        .model("BMW")
                        .price(random.nextInt(100000))
                        .build()
        );

        cars.add(
                Car.builder()
                        .id(2)
                        .model("Mersedes")
                        .price(random.nextInt(100000))
                        .build()
        );

        cars.add(
                Car.builder()
                        .id(3)
                        .model("Tesla")
                        .price(random.nextInt(100000))
                        .build()
        );

        cars.add(
                Car.builder()
                        .id(4)
                        .model("Lexus")
                        .price(random.nextInt(100000))
                        .build()
        );
    }

    @Override
    public void add(Car object) {
        this.cars.add(object);
    }

    @Override
    public Car getById(int id) {
        return cars.stream().filter(car -> car.getId() == id).findFirst().orElseThrow();
    }

    @Override
    public void deleteById(int id) {
        cars.removeIf(c -> c.getId() == id);
    }

    @Override
    public List<Car> getList() {
        return this.cars;
    }
}

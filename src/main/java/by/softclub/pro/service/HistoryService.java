package by.softclub.pro.service;

import by.softclub.pro.domain.Car;

public interface HistoryService extends BaseOperationService<Car> {
}

package by.softclub.pro.service;

import by.softclub.pro.domain.Car;

public interface StorageService extends BaseOperationService<Car> {
}

package by.softclub.pro.service;

import by.softclub.pro.domain.Car;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class HistoryServiceImpl implements HistoryService {

    private List<Car> cars = new ArrayList<>();

    @Override
    public void add(Car object) {
        this.cars.add(object);
    }

    @Override
    public Car getById(int id) {
        return cars.stream().filter(car -> car.getId() == id).findFirst().orElseThrow();
    }

    @Override
    public void deleteById(int id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Car> getList() {
        return this.cars;
    }
}
